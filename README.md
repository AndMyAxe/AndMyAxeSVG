<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

These are SVG images for use by AndMyAxe projects. Or whatever else you want to use them with. They are all licensed CC-BY.

PNG versions are listed in the pngs folder.

# Images

<img src="pngs/AndMyAxeParalithode%20Version.png" alt="AndMyAxeParalithode Version" style="height: 200px;"/>
<img src="pngs/AndMyAxe.png" alt="AndMyAxe" style="height: 200px;"/>
<img src="pngs/AndMyAxeHands.png" alt="AndMyAxeHands" style="height: 200px;"/>
<img src="pngs/BlockyHand.png" alt="BlockyHand" style="height: 200px;"/>
<img src="pngs/Paw1.png" alt="Paw1" style="height: 200px;"/>
<img src="pngs/RobotHand1.png" alt="RobotHand1" style="height: 200px;"/>
<img src="pngs/RatFace.png" alt="RatFace" style="height: 200px;"/>
<img src="pngs/RatFaceCircle.png" alt="RatFaceCircle" style="height: 200px;"/>
<img src="pngs/RatReaderFace.png" alt="RatReaderFace" style="height: 200px;"/>
<img src="pngs/RatReaderFaceCircle.png" alt="RatReaderFaceCircle" style="height: 200px;"/>
<img src="pngs/ExecuBot5000V1.png" alt="ExecuBot5000V1" style="height: 200px;"/>
<img src="pngs/ExecuBot5000V2.png" alt="ExecuBot5000V2" style="height: 200px;"/>
<img src="pngs/ExecuBotBeepBoop.png" alt="ExecuBotBeepBoop" style="height: 200px;"/>
<img src="PainLevels/PainLevels0-10.png" alt="PainLevelFaces" style="height: 200px;"/>
<img src="PainLevels/PainLevels0-10BlackLines.png" alt="PainLevelFacesBlackLines" style="height: 200px;"/>

# Credits

@paralithode@mastodon.social made the original image that `AndMyAxeParalithode Version.svg` is based on. Conversion to SVG and all other images were created by @inmysocks using InkScape.
